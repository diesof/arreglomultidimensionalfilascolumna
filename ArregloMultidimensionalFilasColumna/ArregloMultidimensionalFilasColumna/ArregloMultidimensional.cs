﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArregloMultidimensionalFilasColumna
{
    class ArregloMultidimensional
    {
        private int[,] numeros;
        private int filas, columnas;

        public void IngresarFilasColumnasArreglo()
        {
            Console.Write("Ingrese las filas que tendrá el arreglo: ");
            filas = int.Parse(Console.ReadLine());

            Console.Write("Ingrese las columnas que tendrá el arreglo: ");
            columnas = int.Parse(Console.ReadLine());

            numeros = new int[filas, columnas];
        }

        public void SolicitarNumeros()
        {
            for (int i = 0; i < filas; i++)
            {
                for (int j = 0; j < columnas; j++)
                {
                    Console.Write($"Ingrese un número en la posición [{i},{j}]: ");
                    numeros[i, j] = int.Parse(Console.ReadLine());
                }
            }
        }

        public void MostrarResultados()
        {
            Console.WriteLine("**********Valores de la primera y la última fila**********".ToUpper());

            //Mostrar Filas de acuerdo a los requerimientos
            for (int i = 0; i < filas; i++)
            {
                for (int j = 0; j < columnas; j++)
                {
                    if (i == 0 || i == filas - 1)
                    {
                        Console.WriteLine($"Valor en la posición[{i},{j}] = {numeros[i, j]}");
                    }
                }
            }

            Console.WriteLine("\n");

            Console.WriteLine("**********VALORES DE LA PRIMERA COLUMNA**********");

            //Mostrar Columna de acuerdo a los requerimientos
            for (int i = 0; i < filas; i++)
            {
                Console.WriteLine($"Valor en la posición [{i},{0}] = {numeros[i, 0]}");
            }
        }
    }
}